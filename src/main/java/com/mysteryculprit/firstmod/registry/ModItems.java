package com.mysteryculprit.firstmod.registry;

import com.mysteryculprit.firstmod.Experiments;
import com.mysteryculprit.firstmod.armor.ModArmorMaterials;
import com.mysteryculprit.firstmod.tool.ModAxeItem;
import com.mysteryculprit.firstmod.tool.ModHoeItem;
import com.mysteryculprit.firstmod.tool.ModPickaxeItem;
import com.mysteryculprit.firstmod.tool.ModToolMaterials;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.*;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModItems {

    //ITEM DEFINITIONS

    //Items
    public static final Item RUBY = new Item(new Item.Settings().group(Experiments.ITEM_GROUP));

    //Tools
    public static final Item RUBY_SWORD = new SwordItem(ModToolMaterials.RUBY, 3, -2.4F, (new Item.Settings()).group(Experiments.ITEM_GROUP));
    public static final Item RUBY_SHOVEL = new ShovelItem(ModToolMaterials.RUBY, 1.5F, -3.0F, (new Item.Settings()).group(Experiments.ITEM_GROUP));
    public static final Item RUBY_PICKAXE = new ModPickaxeItem(ModToolMaterials.RUBY, 1, -2.8F, (new Item.Settings()).group(Experiments.ITEM_GROUP));
    public static final Item RUBY_AXE = new ModAxeItem(ModToolMaterials.RUBY, 5, -3.0F, (new Item.Settings()).group(Experiments.ITEM_GROUP));
    public static final Item RUBY_HOE = new ModHoeItem(ModToolMaterials.RUBY, -3, 0.0F, (new Item.Settings()).group(Experiments.ITEM_GROUP));

    //Block Items
    public static final BlockItem RUBY_BLOCK = new BlockItem(ModBlocks.RUBY_BLOCK, new Item.Settings().group(Experiments.ITEM_GROUP));
    public static final BlockItem RUBY_ORE = new BlockItem(ModBlocks.RUBY_ORE, new Item.Settings().group(Experiments.ITEM_GROUP));

    //Armor Items
    public static final Item RUBY_HELMET = new ArmorItem(ModArmorMaterials.RUBY, EquipmentSlot.HEAD, new Item.Settings().group(Experiments.ITEM_GROUP));
    public static final Item RUBY_CHESTPLATE = new ArmorItem(ModArmorMaterials.RUBY, EquipmentSlot.CHEST, new Item.Settings().group(Experiments.ITEM_GROUP));
    public static final Item RUBY_LEGGINGS = new ArmorItem(ModArmorMaterials.RUBY, EquipmentSlot.LEGS, new Item.Settings().group(Experiments.ITEM_GROUP));
    public static final Item RUBY_BOOTS = new ArmorItem(ModArmorMaterials.RUBY, EquipmentSlot.FEET, new Item.Settings().group(Experiments.ITEM_GROUP));

    public static final Item RUBY_HORSE_ARMOR = new HorseArmorItem(11,"ruby", new Item.Settings().group(Experiments.ITEM_GROUP));

    public static void registerItems(){

        //Items
        Registry.register(Registry.ITEM, new Identifier(Experiments.MOD_ID, "ruby"), RUBY);

        Registry.register(Registry.ITEM, new Identifier(Experiments.MOD_ID, "ruby_sword"), RUBY_SWORD);
        Registry.register(Registry.ITEM, new Identifier(Experiments.MOD_ID, "ruby_shovel"), RUBY_SHOVEL);
        Registry.register(Registry.ITEM, new Identifier(Experiments.MOD_ID, "ruby_pickaxe"), RUBY_PICKAXE);
        Registry.register(Registry.ITEM, new Identifier(Experiments.MOD_ID, "ruby_axe"), RUBY_AXE);
        Registry.register(Registry.ITEM, new Identifier(Experiments.MOD_ID, "ruby_hoe"), RUBY_HOE);

        //Block Items
        Registry.register(Registry.ITEM, new Identifier(Experiments.MOD_ID, "ruby_block"), RUBY_BLOCK);
        Registry.register(Registry.ITEM, new Identifier(Experiments.MOD_ID, "ruby_ore"), RUBY_ORE);

        //Armor Items
        Registry.register(Registry.ITEM, new Identifier(Experiments.MOD_ID, "ruby_helmet"), RUBY_HELMET);
        Registry.register(Registry.ITEM, new Identifier(Experiments.MOD_ID, "ruby_chestplate"), RUBY_CHESTPLATE);
        Registry.register(Registry.ITEM, new Identifier(Experiments.MOD_ID, "ruby_leggings"), RUBY_LEGGINGS);
        Registry.register(Registry.ITEM, new Identifier(Experiments.MOD_ID, "ruby_boots"), RUBY_BOOTS);

        Registry.register(Registry.ITEM, new Identifier(Experiments.MOD_ID, "ruby_horse_armor"), RUBY_HORSE_ARMOR);
    }
}
