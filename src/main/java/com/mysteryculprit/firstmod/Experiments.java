package com.mysteryculprit.firstmod;

import com.mysteryculprit.firstmod.registry.ModBlocks;
import com.mysteryculprit.firstmod.registry.ModItems;
import com.mysteryculprit.firstmod.registry.ModOres;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.world.gen.decorator.Decorator;
import net.minecraft.world.gen.decorator.RangeDecoratorConfig;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;

public class Experiments implements ModInitializer {

    public static final String MOD_ID = "experiments";

    // Add Custom Creative Tab
    public static final ItemGroup ITEM_GROUP = FabricItemGroupBuilder.build(
            new Identifier(MOD_ID, "rubystuff"),
            () -> new ItemStack(ModItems.RUBY));

    // Initialisation
    @Override
    public void onInitialize() {
        //Register Stuff
        ModItems.registerItems();
        ModBlocks.registerBlocks();
        ModOres.registerOres();
    }
}
